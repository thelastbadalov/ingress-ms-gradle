package az.ingresss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngressMsGradleApplication {

    public static void main(String[] args) {
        SpringApplication.run(IngressMsGradleApplication.class, args);
    }

}
