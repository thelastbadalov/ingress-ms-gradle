FROM openjdk:17
COPY build/libs/ingress-ms-gradle-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
EXPOSE 8080
ENTRYPOINT ["java"]
CMD ["-jar", "/app/ingress-ms-gradle-0.0.1-SNAPSHOT.jar"]
